﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OOPFileSystem.Structures
{
    public class FileStructure
    {
        public string FileName { get; set; }

        public Dictionary<int, string> ContentOfFile { get; set; }

        public FileStructure(string filename, Dictionary<int,string> contentoffile) {
            FileName = filename;
            ContentOfFile = contentoffile;
        }

        public int GetSizeOfFile()
        {
            int size = 0;
            foreach (KeyValuePair<int, string> content in ContentOfFile)
            {
                size += content.Value.Length;
            }
            if(ContentOfFile.Count != 0)size += ContentOfFile.Keys.Max();
            return size;
        }
    }
}
