﻿using OOPFileSystem.Structures;

namespace OOPFileSystem
{
    public class Exist
    {
        public static bool Folder(string path, FolderStructure current)
        {
            foreach (var folder in current.Children)
            {
                if (folder.Name == path)
                {
                    return true;
                }
            }
            return false;
        }

        public static bool File(string fileName, FolderStructure folder)
        {
            foreach (var file in folder.Files)
            {
                if (file.FileName == fileName)
                {
                    return true;
                }
            }
            return false;
        }

        public static FileStructure GetFileFromName(string path, FolderStructure current) {
            foreach(FileStructure file in current.Files)
            {
                if(file.FileName == path)return file;
            }
            throw new NotImplementedException();
        }
    }
}
