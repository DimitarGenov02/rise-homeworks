﻿using OOPFileSystem.FilesModels;
using OOPFileSystem.Structures;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OOPFileSystem.PipeLineOptions
{
    public class PipeLineFunctions
    {
        public static string PipeLineInplementation(string input, FolderStructure current_folder)
        {
            string result = string.Empty;

            string[] pipeLine = input.Split('|');

            string[] first_command = pipeLine[0].Split(' ');

            string[] second_command = pipeLine[1].Split(' ');

            switch (first_command[0] , second_command.Length)
            {
                case ("ls" , 3):
                    if (first_command[1] == "--sorted") result = WCFunction.WCFunctionTextImplementation(LsFunction.ShowContentOfFolderSortedDecending(current_folder));
                    else result = WCFunction.WCFunctionTextImplementation(LsFunction.ShowContentOfFolder(current_folder));
                    break;
                case ("cat", 3):
                    if (Exist.File(first_command[1], current_folder))
                    {
                        FileStructure found_file = Exist.GetFileFromName(first_command[1], current_folder);
                       result = WCFunction.WCFunctionTextImplementation(CatFunction.ShowContentOfFile(found_file));
                    }
                    break;
                case ("tail", 3):
                    if (Exist.File(first_command[1], current_folder))
                    {
                        FileStructure found_file1 = Exist.GetFileFromName(first_command[1], current_folder);
                        if (first_command.Length == 4) result = WCFunction.WCFunctionTextImplementation(TailFunction.ShowTailOfFile(found_file1, int.Parse(first_command[3])));
                        else result = WCFunction.WCFunctionTextImplementation(TailFunction.ShowTailOfFile(found_file1, 0));
                    }
                    break;
                case ("ls", 4):
                    if (first_command[1] == "--sorted") result = WCFunction.WCFunctionLine(LsFunction.ShowContentOfFolderSortedDecending(current_folder));
                    else result = WCFunction.WCFunctionLine(LsFunction.ShowContentOfFolder(current_folder));
                    break;
                case ("cat", 4):
                    if (Exist.File(first_command[1], current_folder))
                    {
                        FileStructure found_file = Exist.GetFileFromName(first_command[1], current_folder);
                        result = WCFunction.WCFunctionLine(CatFunction.ShowContentOfFile(found_file));
                    }
                    break;
                case ("tail", 4):
                    if (Exist.File(first_command[1], current_folder))
                    {
                        FileStructure found_file1 = Exist.GetFileFromName(first_command[1], current_folder);
                        if (first_command.Length == 4) result = WCFunction.WCFunctionLine(TailFunction.ShowTailOfFile(found_file1, int.Parse(first_command[3])));
                        else result = WCFunction.WCFunctionLine(TailFunction.ShowTailOfFile(found_file1, 0));
                    }
                    break;
            }
            return result;
        }

        public static bool hasPipeLine(string input)
        {
            if(input.Contains('|'))return true;
            return false;
        }
    }
}
