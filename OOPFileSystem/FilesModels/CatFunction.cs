﻿using OOPFileSystem.Structures;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OOPFileSystem.FilesModels
{
    public class CatFunction
    {
        public static string ShowContentOfFile(FileStructure file)
        {
            string result = $"{file.FileName}:\n";

            if(file.ContentOfFile.Count == 0)return $"File {file.FileName} is Empty\n";

            for (int i = 1;i <= file.ContentOfFile.Keys.Max(); i++)
            {
                if (file.ContentOfFile.ContainsKey(i)) result += file.ContentOfFile[i].ToString() + "\n";
                else result += "\n";
            }

            return result;
        }
    }
}
