﻿using OOPFileSystem.Structures;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OOPFileSystem.FilesModels
{
    public class LsFunction
    {
        public static string ShowContentOfFolder(FolderStructure currentFolder)
        {
            string result = $"Files in {currentFolder.Name}:\n";

            foreach(var folder in currentFolder.Children)
            {
                result += folder.Name + " - Folder\n";
            }

            foreach (var file in currentFolder.Files)
            {
                result += file.FileName + " - File\n";
            }

            if(result == $"Files in {currentFolder.Name}:\n")
            {
                result += "None\n";
            }

            return result;
        }

        public static string ShowContentOfFolderSortedDecending(FolderStructure currentFolder)
        {
            string result = $"Files in {currentFolder.Name} sorted by size:\n";

            Dictionary<Tuple<int,int>,string> collection_for_sorting = new Dictionary<Tuple<int, int>,string>();

            int size,index = 1;

            foreach (FolderStructure folder in currentFolder.Children)
            {
                size = folder.GetSizeOfFolder();
                collection_for_sorting.Add(new Tuple<int, int>(size,index), $"{folder.Name} - Folder - {size} bytes\n");
                index++;
            }

            foreach (FileStructure file in currentFolder.Files)
            {
                size = file.GetSizeOfFile();
                collection_for_sorting.Add(new Tuple<int, int>(size, index), $"{file.FileName} - File - {size} bytes\n");
                index++;
            }

            var sorted_dictionary = collection_for_sorting.OrderByDescending(key => key.Key);

            foreach (KeyValuePair<Tuple<int, int>,string> pair in sorted_dictionary)
            {
                result += pair.Value;
            }

            if (result == $"Files in {currentFolder.Name} sorted by size:\n")
            {
                result += "None - 0 bytes\n";
            }

            return result;
        }
    }
}
